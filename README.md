How can the detectIVe help you with?

- Fast and easy import of HEKA Patchmaster .asc data.
- Cleanup of imported .asc data.
- Calculation of various measurement parameters such as the current density.
- Easy way to save and store data for later use as a single .rdata file.
- Create a large set of different beautifull plots, such as current density boxplots or current density - voltage relation plots using ggplot2 and an easy to use GUI.
- Calculate and display key parameters and statistics for those plots.
- Save plots as bitmaps, vector graphics or even .rdata for later use.


How to setup the detecIVe?
1. Make sure Microsoft R Open 4.02 is installed and selected as active R version. (Tools/Global Options/ R Version)
2. Download the files in the repository
3. Open the prepareDetectIVe.r file and run it to install checkpoint, vctr and shiny, create a checkpoint and then install all other needed packages. If asked if you want to restart RStudio press "No". 
4  Once all packages are installed start the application by clicking on the "Run App" Button on the top right.
5. Click on Settings then Packages and then checl the installed Packages. If one is If asked if you want to restart R Studio press no.
